import React from "react";
import styles from "../../styles/addOffer.module.css";
import HotelDetails from "../../components/HotelDetail";
import Checkbox from "../../components/Checkbox";
import Button from "../../components/Button";
const AddOffer: React.FC = () => {
  return (
    <div className={styles.container}>
      <div className={styles.wrapper}>
        <div className={styles.selects}>
          <span className={styles.select_title}>Отель</span>
          <select className={styles.select_item} name="cars" id="cars">
            <option value="volvo">Hyatt Regency</option>
            <option value="Dostuk">Dostuk</option>
            <option value="Jannat">Jannat</option>
          </select>

          <span className={styles.select_title}>Номер</span>
          <select className={styles.select_item} name="cars" id="cars">
            <option value="volvo">205</option>
            <option value="saab">210</option>
            <option value="mercedes">208</option>
            <option value="audi">200</option>
          </select>
        </div>
      </div>
      <div>
        <HotelDetails />
      </div>
      <div>
        <h2 className={styles.services_title}>Удобства и услуги</h2>
        <div className={styles.services}>
          <Checkbox
            onChangeFunc={(e) => {
              console.log(e.target.checked);
            }}
          >
            Телевизор с плоским экраном
          </Checkbox>
          <Checkbox
            onChangeFunc={(e) => {
              console.log(e.target.checked);
            }}
          >
            Звукоизоляция
          </Checkbox>
          <Checkbox
            onChangeFunc={(e) => {
              console.log(e.target.checked);
            }}
          >
            Халат
          </Checkbox>
          <Checkbox
            onChangeFunc={(e) => {
              console.log(e.target.checked);
            }}
          >
            Маникюр
          </Checkbox>
          <div className={styles.price}>
            <Checkbox
              onChangeFunc={(e) => {
                console.log(e.target.checked);
              }}
            >
              Спа и оздоровительный центр
            </Checkbox>
            <input type="text" className={styles.priceInput} />
            <input type="text" className={styles.priceInput} />
          </div>
          <Checkbox
            onChangeFunc={(e) => {
              console.log(e.target.checked);
            }}
          >
            Пресс для брюк
          </Checkbox>
          <div className={styles.price}>
            <Checkbox
              onChangeFunc={(e) => {
                console.log(e.target.checked);
              }}
            >
              Трансфер в аэропорт
            </Checkbox>
            <input type="text" className={styles.priceInput} />
            <input type="text" className={styles.priceInput} />
          </div>
          <Checkbox
            onChangeFunc={(e) => {
              console.log(e.target.checked);
            }}
          >
            Бар
          </Checkbox>
          <Checkbox
            onChangeFunc={(e) => {
              console.log(e.target.checked);
            }}
          >
            Ресторан
          </Checkbox>
          <Checkbox
            onChangeFunc={(e) => {
              console.log(e.target.checked);
            }}
          >
            Кабельные каналы
          </Checkbox>
        </div>
      </div>
      <div className={styles.capacity}>
        <h2 className={styles.capacity_title}>Вместимость</h2>
        <div className={styles.capacity_item}>
          <img
            className={styles.capacity_item_img}
            src=".././icons/person.png"
            alt=""
          />
          <input
            value="5400"
            type="text"
            className={styles.capacity_item_form}
          />
          <span className={styles.capacity_item_text}>за 1 ночь</span>
        </div>
        <div className={styles.capacity_item}>
          <img
            className={styles.capacity_item_img}
            src=".././icons/person.png"
            alt=""
          />
          <input
            value="4200"
            type="text"
            className={styles.capacity_item_form}
          />
          <span className={styles.capacity_item_text}>за 1 ночь</span>
        </div>
        <div className={styles.capacity_item}>
          <img
            className={styles.capacity_item_img}
            src=".././icons/person.png"
            alt=""
          />
          <input
            value="3100"
            type="text"
            className={styles.capacity_item_form}
          />
          <span className={styles.capacity_item_text}>за 1 ночь</span>
        </div>
        <Button
          onclickFunc={() => alert("Hello")}
          className={`bigBtn ${styles.saveBtn}`}
        >
          Создать{" "}
        </Button>
      </div>
    </div>
  );
};

export default AddOffer;
