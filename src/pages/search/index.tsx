import React, { ReactChildren } from "react";
import Button from "../../components/Button";
import styles from "../../styles/search.module.css";
const Search: React.FC = () => {
  return (
    <div className={styles.container}>
              <div className={styles.wrapper}>
      <div className={styles.selects}>
        <span className={styles.select_title}>Отель</span>
        <select className={styles.select_item} name="cars" id="cars">
          <option value="volvo">Hyatt Regency</option>
          <option value="Dostuk">Dostuk</option>
          <option value="Jannat">Jannat</option>
        </select>

        <span className={styles.select_title}>Номер</span>
        <select className={styles.select_item} name="cars" id="cars">
          <option value="volvo">205</option>
          <option value="saab">210</option>
          <option value="mercedes">208</option>
          <option value="audi">200</option>
        </select>
        <span className={styles.select_title}>Отель</span>
        <select className={styles.select_item} name="cars" id="cars">
          <option value="volvo">Hyatt Regency</option>
          <option value="Dostuk">Dostuk</option>
          <option value="Jannat">Jannat</option>
        </select>

        <span className={styles.select_title}>Номер</span>
        <select className={styles.select_item} name="cars" id="cars">
          <option value="volvo">205</option>
          <option value="saab">210</option>
          <option value="mercedes">208</option>
          <option value="audi">200</option>
        </select>
        </div>

        <div className={styles.search_place}>
            <div className={styles.search_place_item}>
                <span className={styles.sleeping_place_title}>Спальное место</span>
                <select className={styles.select_sleeping_place} name="cars" id="cars">
                    <option value="volvo">205</option>
                    <option value="saab">210</option>
                    <option value="mercedes">208</option>
                    <option value="audi">200</option>
                </select>
                <span className={styles.sleeping_place_title}>Количество</span>
                <input type="text" className={styles.sleeping_place_count_input} name="" id="" />
                    .kg
            </div>

            <div className={styles.search_place_item}>
                <span className={styles.sleeping_place_title}>Спальное место</span>
                <select className={styles.select_sleeping_place} name="cars" id="cars">
                    <option value="volvo">205</option>
                    <option value="saab">210</option>
                    <option value="mercedes">208</option>
                    <option value="audi">200</option>
                </select>
                <span className={styles.sleeping_place_title}>Количество</span>
                <input type="number" className={styles.sleeping_place_count_input} name="" id="" />
                    .kg
            </div>
            <img src=".././icons/plus.png" className={styles.plus_btn}  alt="" />
        </div>
        <Button onclickFunc={() => console.log('search')} className={`smallBtn ${styles.search_btn}`}>Найти</Button>
        <div className={styles.founded} >
          <span className={styles.founded_title}>Найдены 5 номеров</span>
            <div className={styles.founded_numbers}>203</div>
        </div>
      </div>
      
    </div>
  );
};

export default Search;
