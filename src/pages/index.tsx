import styles from "../styles/Home.module.css";
import SmallButton from "../components/Button";
import Link from "next/link";

export default function Home() {
  const openModal = () => {
    alert("Open modal");
  };
  return (
    <>
      <div className={styles.container}>
        <Link href={`/add`}>
          <a>
            <SmallButton className={`smallBtn ${styles.addBtn}`}>
              Add
            </SmallButton>
          </a>
        </Link>

        <div className={styles.wrapper}>
          <div className={styles.numbers}>
            <img src="./icons/corona.svg" className={styles.corona} alt="" />
            <h3 className={styles.hotel_number} align="center">
              201
            </h3>
            <h4 className={styles.number_type} align="center">
              STUD
            </h4>
          </div>
          <ul className={styles.description_list}>
            <li className={styles.description_item}>
              <img
                src="./icons/bed.png"
                className={styles.description_item_img}
                alt=""
              />
              <p className={styles.description_item_text}>
                1 двуспальная кровать (180х200) + 1 односпальная кровать
                (90х200)
              </p>
            </li>
            <li className={styles.description_item}>
              <img
                src="./icons/window.png"
                className={styles.description_item_img}
                alt=""
              />
              <p className={styles.description_item_text}>
                Вид на достопримичательность, вид на море, вид на горы
              </p>
            </li>
            <li className={styles.description_item}>
              <img
                src="./icons/roomsicon.png"
                className={styles.description_item_img}
                alt=""
              />
              <p className={styles.description_item_text}>
                1 Спальня, 1 Зал, 1 Ванная комната, 1 Уборная
              </p>
            </li>
            <li className={styles.description_item}>
              <img
                src="./icons/square.png"
                className={styles.description_item_img}
                alt=""
              />
              <p className={styles.description_item_text}>55 м</p>
            </li>
          </ul>
          <ul className={styles.price_list}>
            <li
              className={`${styles.price_item} ${styles.price_icon_settings}`}
            >
              <img
                src="./icons/settings.png"
                className={styles.price_icon}
                alt=""
              />
              <span className={styles.price}> 2 платные</span>
            </li>
            <li className={styles.price_item}>
              <img
                src="./icons/person.png"
                className={styles.price_icon}
                alt=""
              />
              <span className={styles.price}> 5400c</span>
            </li>
            <li className={styles.price_item}>
              <img
                src="./icons/person.png"
                className={styles.price_icon}
                alt=""
              />
              <span className={styles.price}> 4200c </span>
            </li>
            <li className={styles.price_item}>
              <img
                src="./icons/person.png"
                className={styles.price_icon}
                alt=""
              />
              <span className={styles.price}> 3200c</span>
            </li>
          </ul>
          <div>
            <img
              onClick={openModal}
              className={styles.more_btn}
              src="./icons/more.png"
              alt=""
            />
            <div className={styles.more_content}>
              <Link href="/edit">
                <a> Edit</a>
              </Link>
              <Link href="/more">
                <a> More</a>
              </Link>
              <Link href="/publish">
                <a> Publish</a>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
