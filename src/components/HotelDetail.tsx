import React from 'react'
import styles from '../styles/hotelDetail.module.css'
import ImageSlider from '../components/Slider'
const HotelDetails: React.FC = () => {
    return (
        <div className={styles.container}>
                <div className={styles.slider}>
                    {/* <ImageSlider/> */}
                    <img src="./icons/sliderbg.png" className={styles.slider_image} alt="" />
                </div>
                <div className={styles.details}>
                    <div className={styles.hotel_item_head}>
                        <img className={styles.hotel_item_head_img} src="./icons/coronasmall.png" alt="" />
                        <h3 className={styles.hotel_item_head_title}>De Luxe</h3>
                        <div className={styles.hotel_item_description_list}>
                            <div className={styles.hotel_item_description_title}>
                                Спальное место:
                            </div>
                            <div className={styles.hotel_item_description_content}>
                                1 двуспальная кровать (180х200) + 1 односпальная кровать (90х200)
                            </div>
                            <div className={styles.hotel_item_description_title}>
                                Виды из окна:
                            </div>
                            <div className={styles.hotel_item_description_content}>
                                Вид на море, вид на пляж
                            </div>
                            <div className={styles.hotel_item_description_title}>
                                Комнаты:
                            </div>
                            <div className={styles.hotel_item_description_content}>
                                1 Спальня, 1 Зал, 1 Ванная комната, 1 Уборная
                            </div>
                            <div className={styles.hotel_item_description_title}>
                                Площадь:
                            </div>
                            <div className={styles.hotel_item_description_content}>
                                55
                            </div>
                            <div className={styles.hotel_item_description_title}>
                                Типы:
                            </div>
                            <div className={styles.hotel_item_description_content}>
                                Studio
                            </div>
                            <div className={styles.hotel_item_description_title}>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    )
}

export default HotelDetails