import React, { ReactChildren } from 'react'
interface ButtonProps {
    children: React.ReactNode | ReactChildren;
    className: string;
    onclickFunc?:  React.MouseEventHandler<HTMLButtonElement>
}

const Button: React.FC<ButtonProps> = ({children, className, onclickFunc} : ButtonProps)  => {
    return (
        <React.Fragment>
            <button onClick={onclickFunc} className={className}>{children}</button>
        </React.Fragment>
    )
}

export default  Button