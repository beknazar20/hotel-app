import styles from '../styles/slider.module.css'

import React, {  useState } from 'react'


const ImageSlider: React.FC = ()  => {
  const [images, setImages] = useState([
      "https://s3.us-east-2.amazonaws.com/dzuz14/thumbnails/aurora.jpg",
      "https://s3.us-east-2.amazonaws.com/dzuz14/thumbnails/canyon.jpg",
      "https://s3.us-east-2.amazonaws.com/dzuz14/thumbnails/city.jpg",
      "https://s3.us-east-2.amazonaws.com/dzuz14/thumbnails/desert.jpg",
      "https://s3.us-east-2.amazonaws.com/dzuz14/thumbnails/mountains.jpg",
      "https://s3.us-east-2.amazonaws.com/dzuz14/thumbnails/redsky.jpg",
      "https://s3.us-east-2.amazonaws.com/dzuz14/thumbnails/sandy-shores.jpg",
      "https://s3.us-east-2.amazonaws.com/dzuz14/thumbnails/tree-of-life.jpg"
  ])
    const [currentIndex, setCurrentIndex] = useState<number>(0)
    const [translateValue, setTranslateValue] = useState<number>(0)


    const slideWidth = () => {
        return document.querySelector('.slide').clientWidth
     }


    const goToPrevSlide = () => {
        if(currentIndex === 0)  return;
        setCurrentIndex(prevState => ({
             currentIndex = prevState.currentIndex - 1,
        }))

        setTranslateValue(prevState => ({
            translateValue = prevState.translateValue + slideWidth()
        }))      
    }

    const goToNextSlide = () => {
        if(currentIndex === images.length - 1) {
            setCurrentIndex(0)
            setTranslateValue(0)
        }

        setCurrentIndex(prevState => ({
            currentIndex = prevState.currentIndex + 1,
       }))

       setTranslateValue(prevState => ({
           translateValue = prevState.translateValue + -(slideWidth())
       }))       
      }


    return (
        <div className="slider">  
        <div className="slider-wrapper"
          style={{
            transform: `translateX(${translateValue}px)`,
            transition: 'transform ease-out 0.45s'
          }}>
            {
              images.map((image, i) => (
                <Slide key={i} image={image} />
              ))
            }
        </div>

        <LeftArrow
         goToPrevSlide={goToPrevSlide}
        />

        <RightArrow
         goToNextSlide={goToNextSlide}
        />
      </div>
    ) 
}

const Slide = ({ image }) => {
    const slideStyles = {
      backgroundImage: `url(${image})`,
      backgroundSize: 'cover',
      backgroundRepeat: 'no-repeat',
      backgroundPosition: '50% 60%'
    }
    return <div className="slide" style={slideStyles}></div>
  }

  const LeftArrow = (props) => {
    return (
      <div className="backArrow arrow" onClick={props.goToPrevSlide}>
        <i className="fa fa-arrow-left fa-2x" aria-hidden="true"></i>
      </div>
    );
  }
  
  
  const RightArrow = (props) => {
    return (
      <div className="nextArrow arrow" onClick={props.goToNextSlide}>
        <i className="fa fa-arrow-right fa-2x" aria-hidden="true"></i>
      </div>
    );
  }

export default  ImageSlider


  
  

// import React from "react";
// import Slider from "react-slick";
// import "slick-carousel/slick/slick.css";
// import "slick-carousel/slick/slick-theme.css";
// export default function SimpleSlider() {
//   var settings = {
//     dots: true,
//     infinite: true,
//     speed: 500,
//     slidesToShow: 1,
//     slidesToScroll: 1
//   };
//   return (
//     <Slider {...settings}>
//       <div >
//         <h3>1</h3>
//       </div>
//       <div>
//         <h3>2</h3>
//       </div>
//       <div>
//         <h3>3</h3>
//       </div>
//       <div>
//         <h3>4</h3>
//       </div>
//       <div>
//         <h3>5</h3>
//       </div>
//       <div>
//       <h3>5</h3>      </div>
//     </Slider>
//   );
// }
 