import React, { ReactChildren } from 'react'
import styles from '../styles/checkbox.module.css'
interface CheckboxProps {
    children: React.ReactNode | ReactChildren;
    onChangeFunc:  React.ChangeEventHandler<HTMLInputElement>
}

const Checkbox: React.FC<CheckboxProps> = ({children,  onChangeFunc} : CheckboxProps)  => {
    return (
        <div className={styles.checkboxs}>
            <input type="checkbox" className={styles.inputCls}   onChange={(e) => onChangeFunc(e)}  />

            <input type="checkbox" className={styles.inputCls}  />
            <span  className={styles.checkbox_label}>{children}</span>        
        </div>
    )
}

export default  Checkbox